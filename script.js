var app = angular.module('imagineApp', ['ngRoute', 'angularMoment', 'angular-web-notification']);
var prefixUrl = "spa/";

app.config(function ($routeProvider) {


    $routeProvider
            .when('/', {
                templateUrl: 'partials/shared/body.html'
            })
            .when('/about', {
                templateUrl: 'partials/about.html'
            });


});

/**
 * Global Configuration
 * 
 */
app.run(function ($rootScope, newsSocket) {
    $rootScope.prefixUrl = '';
    $rootScope.userDetails = null;
    $rootScope.newPostsCounter = 0;
    $rootScope.temporaryPostsCache = [];

    $rootScope.getNewPostsCounter = function () {
        return $rootScope.newPostsCounter ? $rootScope.newPostsCounter : 0;
    }
});

app.controller('cfgController', ['$scope', 'newsSocket', 'defaultService', function ($scope, newsSocket, defaultService) {

        $scope.message = "Hello world";
        $scope.posts = [];
        $scope.post_id = null;
        $scope.initialPostLimit = 10;
        $scope.comments = [];
        $scope.commentsLimit = [];
        $scope.loadingComment = [];
        $scope.addingComment = false;


        newsSocket.on($scope, "message");
        newsSocket.on($scope, "news-post");
        //update comment
//      $scope.$on('postUpdated', function(event, data) {
//         if(typeof data != 'undefined') {
//              // console.log(angular.toJson(data));
//              var activeIndex = $scope.posts.indexOf(data.post);
//              // console.log('Index: ' + activeIndex);
//              $scope.posts[activeIndex].comments.push(data.comment);
//              // $log.info(angular.toJson($scope.posts[activeIndex].comments));
//              $scope.$apply(function() {
//                 $scope.posts[activeIndex].comments;
//              });
//         }
//      });

//load all posts
        var allPostsUrl = 'http://localhost:8080/news-app/public/api/post/1';
        defaultService.allGetRequests(allPostsUrl).
                then(function (posts) {
                    console.log(JSON.stringify(posts));
                    angular.forEach(posts, function (post) {
                        //console.log(JSON.stringify(post));
                        // Split post timestamp into [ Y, M, D, h, m, s ]
                        var t = post.created_at.split(/[- :]/);
                        // Apply each element to the Date function
                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                        d.setTime(d.getTime() - new Date().getTimezoneOffset() * 60 * 1000);

                        // Split comment timestamp into [ Y, M, D, h, m, s ]
                        if (post.comments.length > 0) {
                            var commentcount = 0;
                            angular.forEach(post.comments, function (pcomment) {
                                //alert(pcomment.id)
                                var c = pcomment.created_at.split(/[- :]/);
                                // Apply each element to the Date function
                                var cd = new Date(c[0], c[1] - 1, c[2], c[3], c[4], c[5]);
                                cd.setTime(cd.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
                                post.comments[commentcount].created_at = cd.getTime();
                                commentcount++;
                            });
                        }


                        // console.log(d);
                        if (post.user) {
                            post.user.slug = '/' + prefixUrl + 'profile/' + post.user.slug;
                        }
                        post.created_at = d.getTime();
                        post.likes = post.news_likes.length;
                        $scope.posts.push(post);
                    });
                }).
                then(function (err) {
                    if (typeof err != 'undefined') {
                        console.log(JSON.stringify(err));
                    }
                });

        //load more posts 
        $scope.loadMorePosts = function () {
            $scope.initialPostLimit += 10;
        };

        //load more comments
        $scope.loadMoreComments = function (postId) {
            $scope.loadingComment['loading_comment_' + postId] = true;
            $timeout(function () {
                $scope.commentsLimit['comments_limit_' + postId] += 20;
                $scope.loadingComment['loading_comment_' + postId] = false;
                console.log($scope.commentsLimit['comments_limit_' + postId]);
                return;
            }, 2000);
        };

    }]);

//=================================================== FACTORIES ========================================
app.factory('newsSocket', ['$rootScope', '$window', 'webNotification', function ($rootScope, $window, webNotification) {
        var socket = io('http://localhost:8890');
        return {
            on: function (scope, eventName) {
                socket.on(eventName, function (message) {
                    var messages = JSON.stringify(message.news);
                    message = JSON.parse(messages);
                    angular.forEach(message, function (post) {
                        message = post;
                    });

                    $rootScope.$apply(function () {
                        var t = message.created_at.split(/[- :]/);
                        // Apply each element to the Date function
                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                        d.setTime(d.getTime() - new Date().getTimezoneOffset() * 60 * 1000);

                        // console.log(d);
                        if (message.user) {
                            message.user.slug = '/' + prefixUrl + 'profile/' + message.user.slug;
                        }
                        message.created_at = d.getTime();
                        message.likes = message.news_likes.length;
                        $rootScope.temporaryPostsCache.push(message);
                        $rootScope.newPostsCounter++;
                    });
                    //---- update new notifiation icon ------- //
                    $("#msgnotification").html($rootScope.newPostsCounter);

                    //send a web notification to all users if foodtingy is not the current running tab
                    webNotification.showNotification('News Notification', {
                        body: 'Latest: ' + $rootScope.newPostsCounter + ' News unread',
                        icon: '/' + prefixUrl + 'assets/img/avatar2-sm.jpg',
                        onClick: function onNotificationClicked() {
                            console.log('Notification clicked.');
                            // $window.location.href = '/' + prefixUrl;
                        },
                        autoClose: 7000 //auto close the notification after 7 seconds (you can manually close it via hide function)
                    }, function onShow(error, hide) {
                        if (error) {
                            console.error.bind(console, 'Unable to show notification: ' + error.message);
                        } else {
                            console.log('Notification Shown.');
                        }
                    });//end web notification service
                });
            }
        }
    }]);

/** 
 * Default Service
 *
 */
app.factory('defaultService', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
        return {
            allPutRequests: function (url, params) {
                var defer = $q.defer();
                var promise = defer.promise;
                $http.put(url, {params: params}).success(function (response) {
                    defer.resolve(response);
                })
                        .error(function (error) {
                            defer.resolve(error);
                        });
                //return promise object
                return promise;
            },
            allPostRequests: function (url, params) {
                var defer = $q.defer();
                var promise = defer.promise;
                $http.post(url, {params: params}).success(function (response) {
                    defer.resolve(response);
                })
                        .error(function (error) {
                            defer.resolve(error);
                        });
                //return promise object
                return promise;
            },
            allGetRequests: function (url) {
                var defer = $q.defer();
                var promise = defer.promise;
                $http.get(url).success(function (response) {
                    defer.resolve(response);
                })
                        .error(function (error) {
                            defer.resolve(error)
                        });

                //return promise
                return promise;
            },
            allDeleteRequests: function (url, params) {
                var defer = $q.defer();
                var promise = defer.promise;
                $http.delete(url, {params: params}).success(function (response) {
                    defer.resolve(response);
                })
                        .error(function (error) {
                            defer.resolve(error);
                        });
                //return promise object
                return promise;
            },
            allSecurePostRequests: function (url, params) {
                var defer = $q.defer();
                var promise = defer.promise;
                var result = $http({method: 'POST',
                    url: url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {params: params}
                }).then(function (resp) {
                    defer.resolve(response);
                }, function (err) {
                    defer.resolve(response);
                });
                //return promise 
                return promise;
            }
        };
    }]);

/**
 * Errors Interceptors
 * 
 */
app.factory('httpErrorsInterceptor', ['$q', '$window', function ($q, $window) {
        return {
            responseError: function (response) {
                if (response.status !== undefined && response.status !== 200) {
                    alert('an error occured: ' + response.status);
                    // $window.location.reload();
                }
            }
        };
    }]);